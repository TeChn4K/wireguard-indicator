# Wireguard Indicator

Display a Wireguard indicator for your GTK panel

This application is in early stages of development. There is no stable release yet.

This is not an official Wireguard application!

## Features

- Wireguard interface status in your notification tray
- List /etc/wireguard/\*.conf files and connect
- Show current conf used

## Requirements

- python3 & python-gi
- To have at least one wireguard config file in your `/etc/wireguard`

## Install

- Clone wireguard-indicator folder
- (Optional) Prevent password asking by adding Polkit actions (see below)

## Run

- `python3 wireguard-indicator.py`

## Polkit actions

Wireguard commands need to be run as root, and wireguard-indicator delegate this responsability to Polkit. (https://wiki.archlinux.org/index.php/Polkit)

By default, Polkit will ask for your root password at each wireguard command. But you can specify to not ask for your password at all.

Simply add those two files to your `/usr/share/polkit-1/actions` folder.

_Becareful! Anyone one your computer will be abble to run `wg` and `wg-quick` commands, without root privilege_

**/usr/share/polkit-1/actions/wg.policy**

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE policyconfig PUBLIC
 "-//freedesktop//DTD PolicyKit Policy Configuration 1.0//EN"
 "http://www.freedesktop.org/standards/PolicyKit/1/policyconfig.dtd">

<policyconfig>
  <action id="org.freedesktop.policykit.pkexec.wg">
    <defaults>
      <allow_any>yes</allow_any>
      <allow_inactive>yes</allow_inactive>
      <allow_active>yes</allow_active>
    </defaults>
    <annotate key="org.freedesktop.policykit.exec.path">/usr/bin/wg</annotate>
  </action>
</policyconfig>
```

**/usr/share/polkit-1/actions/wgquick.policy**

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE policyconfig PUBLIC
 "-//freedesktop//DTD PolicyKit Policy Configuration 1.0//EN"
 "http://www.freedesktop.org/standards/PolicyKit/1/policyconfig.dtd">

<policyconfig>
  <action id="org.freedesktop.policykit.pkexec.wgquick">
    <defaults>
      <allow_any>yes</allow_any>
      <allow_inactive>yes</allow_inactive>
      <allow_active>yes</allow_active>
    </defaults>
    <annotate key="org.freedesktop.policykit.exec.path">/usr/bin/wg-quick</annotate>
  </action>
</policyconfig>
```
