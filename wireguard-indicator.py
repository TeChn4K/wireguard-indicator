#!/usr/bin/env python3
import subprocess
import os, sys, pwd, glob, socket
import signal
import threading
import re
import requests, json

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('AppIndicator3', '0.1')

from gi.repository import Gtk, GLib, AppIndicator3

currpath = os.path.dirname(os.path.realpath(__file__))
INDICATOR_ID = 'wireguard-applet'

iconpath = currpath + '/unlock.png'
iconConnectedpath = currpath + '/lock.png'
iconLimitedpath = currpath + '/lock_warn.png'

whoami = pwd.getpwuid(os.getuid())[0]

infos = {
    'interface': ''
}   

class Indicator: 
    def __init__(self):
        self.indicator = AppIndicator3.Indicator.new(INDICATOR_ID, iconpath, AppIndicator3.IndicatorCategory.SYSTEM_SERVICES)
        self.indicator.set_status(AppIndicator3.IndicatorStatus.ACTIVE)       
        self.indicator.set_menu(self.getMenu())

    def getMenu(self):
        menu = Gtk.Menu()

        confSubMenu = Gtk.Menu()

        confList = getConfList()
        for confName in confList:
            menuItem = Gtk.MenuItem(label=confName)
            menuItem.connect('activate', lambda _, confName=confName: connect(confName))
            confSubMenu.append(menuItem)


        self.statusItem = Gtk.MenuItem(label='Initializing', sensitive=False)
        menu.append(self.statusItem)

        menu.append(Gtk.SeparatorMenuItem())
        
        self.connectItem = Gtk.MenuItem(label='Connect', sensitive=True)
        self.connectItem.set_submenu(confSubMenu)
        menu.append(self.connectItem)


        self.disconnItem = Gtk.MenuItem(label='Disconnect', sensitive=False)
        self.disconnItem.connect('activate', lambda _: disconnect())
        menu.append(self.disconnItem)

        menu.append(Gtk.SeparatorMenuItem())

        item_quit = Gtk.MenuItem(label='Quit')
        item_quit.connect('activate', stop)
        menu.append(item_quit)

        menu.show_all()
        return menu

    def setConnected(self):
        """Set status as CONNECTED"""
        self.updateStatusText('Connected - ' + infos['interface'])
        self.indicator.set_icon(iconConnectedpath)
        self.connectItem.set_sensitive(False)
        self.disconnItem.set_sensitive(True)

    def setLimited(self):
        """Set status as LIMITED"""
        self.updateStatusText('Internet down - ' + infos['interface'])
        self.indicator.set_icon(iconLimitedpath)
        self.connectItem.set_sensitive(False)
        self.disconnItem.set_sensitive(True)

    def setConnecting(self):
        """Set status as CONNECTING"""
        self.updateStatusText('Connecting ...')
        self.connectItem.set_sensitive(False)
        self.disconnItem.set_sensitive(True)

    def setDisconnected(self):
        """Set status as DISCONNECTED"""
        self.updateStatusText('Disconnected')
        self.indicator.set_icon(iconpath)
        self.connectItem.set_sensitive(True)
        self.disconnItem.set_sensitive(False)

    def setDisconnecting(self):
        """Set status as DISCONNECTING"""
        self.updateStatusText('Disconnecting ...')
        self.connectItem.set_sensitive(True)
        self.disconnItem.set_sensitive(False)


    def updateStatusText(self, value):
        self.statusItem.set_label(value)



def connect(confName):
    """Connect to a wireguard interface"""
    indicator.setConnecting()

    cmd = ThreadedCmd(command='wg-quick up ' + confName, callback=lambda _: updateStatus())
    cmd.start()

def disconnect():
    """Disconnect from a wireguard interface"""
    indicator.setDisconnecting()

    cmd = ThreadedCmd(command='wg-quick down ' + infos['interface'], callback=lambda _: updateStatus())
    cmd.start()

def stop(source):
    Gtk.main_quit()


def getConfList():
    """Return a list of available Wireguard interfaces"""
    txtfiles = []
    for file in glob.glob("/etc/wireguard/*.conf"):
        txtfiles.append(os.path.splitext(os.path.basename(file))[0])
    return list(sorted(set(txtfiles)))


def updateStatus():
    """Update indicator status"""
    cmd = ThreadedCmd(command='wg show', callback=callbackStatus)
    cmd.start()

    return True

def callbackStatus(output):
    """Check status from Wireguard output and update indicator"""
    infos = extractInfos(output)

    if (infos['interface'] != ''):
        if(checkInternet()):
            GLib.idle_add(indicator.setConnected)
        else:
            GLib.idle_add(indicator.setLimited)
        
    else:
        GLib.idle_add(indicator.setDisconnected)
        
def extractInfos(text):
    """Read Wireguard output and set global variables"""
    m = re.search('interface: (.+)', text)
    infos['interface'] = m.group(1) if m else ''

    return infos

def checkInternet(host="9.9.9.9", port=53, timeout=3):
    """Is internet running good?"""
    try:
       socket.setdefaulttimeout(timeout)
       socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
       return True
    except Exception as e:
       return False


class ThreadedCmd( threading.Thread ):
    def __init__(self, command, callback=None):
        self.command = command
        self.callback = callback
        threading.Thread.__init__(self)
        self.daemon = True

    def run(self):
        self.process = subprocess.Popen('pkexec ' + self.command, stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
                
        stdout, stderr = self.process.communicate()
        output = stdout.decode()

        if self.callback is not None:
            self.callback(output)

    def stop(self):
        try:
            if self.process is not None:
                os.killpg(os.getpgid(self.process.pid), signal.SIGTERM)
        # Silently fail if process already exited or no exists anymore
        except (OSError, AttributeError) as e:
            pass

if __name__ == '__main__':
    indicator = Indicator()

    # Update status now, then every 3s
    updateStatus()
    GLib.timeout_add(3000, updateStatus)

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    Gtk.main()